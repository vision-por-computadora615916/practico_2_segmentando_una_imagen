# Práctico_2_Segmentando_una_imagen

# Segmentación de Imágenes

[Segmentado](https://gitlab.com/vision-por-computadora615916/practico_2_segmentando_una_imagen/-/blob/main/segmentado.py) es un proyecto en Python que convierte una imagen en blanco y negro manteniendo solo las partes que están dentro de un cierto rango de color. En este caso, se busca segmentar las hojas verdes de la imagen y convertirlas en blanco y negro. Ademas se utilizó OpenCV para leer y crear imagenes.



## Detalles del Proyecto

- `segmentacion.py`: Contiene el código Python para la segmentación de la imagen.
- `hoja.png`: La imagen de entrada que se utilizará para la segmentación.
- `restultado1.png`: La imagen transoformada en blanco y negro.
- `resutado2.png`: La imagen resultante después de aplicar el proceso de segmentación.

