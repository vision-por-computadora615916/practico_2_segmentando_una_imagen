#!/usr/bin/env python
#-*-coding: utf-8-*-

import cv2

img = cv2.imread('hoja.png',0)
cv2.imwrite('resultado1.png',img)

for row in range(len(img)):
	for col in range(len(img[row])):
		if img[row][col] <= 200 :
			img[row][col] = 0
		else:
			img[row][col] = 255

cv2.imwrite('resultado2.png',img)
